package com.videodownload.practicletest

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.google.gson.Gson
import com.videodownload.practicletest.adapter.VideoDownloadAdapter
import com.videodownload.practicletest.model.VideoData
import kotlinx.android.synthetic.main.activity_vedio_list.*

class VideoListActivity : AppCompatActivity() {
    var downloadableContent: String = ""
    var videoData: VideoData? = null
    var adapter: VideoDownloadAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vedio_list)

        adapter = VideoDownloadAdapter(this, ArrayList())
        rvVideoList.adapter = adapter

        Handler().post {
            downloadableContent = readFileFromAsset(this)
            videoData = Gson().fromJson(downloadableContent, VideoData::class.java)
            videoData?.videos?.let {
                adapter?.list?.addAll(it)
                if (adapter?.list?.isNullOrEmpty() == false) {
                    adapter?.list?.get(0)?.selected = true
                }
                adapter?.notifyDataSetChanged()
            }
        }

        rvVideoList.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    rvVideoList.post {
                        val position =
                            (rvVideoList.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                        val lastPosition =
                            (rvVideoList.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
                        Log.e("VideoListActivity", "$position: $lastPosition")
                        if (lastPosition == (adapter?.list?.size ?: 0) - 1) {
                            adapter?.selectItemForPlay(lastPosition)
                        } else {
                            adapter?.selectItemForPlay(position)
                        }
                        adapter?.notifyDataSetChanged()
                    }
                }
            }
        })
    }
}
