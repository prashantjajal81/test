package com.videodownload.practicletest.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.videodownload.practicletest.R
import com.videodownload.practicletest.model.VideoObj
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_video.*

class VideoDownloadAdapter(val context: Context, val list: ArrayList<VideoObj>) :
    RecyclerView.Adapter<VideoDownloadAdapter.VideoHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): VideoHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_video, p0, false)

        return VideoHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: VideoHolder, pos: Int) {
        holder.bind(list[pos])
    }

    class VideoHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(videoObj: VideoObj) {
            viewVideo.videoObj = videoObj
        }
    }

    fun selectItemForPlay(pos: Int) {
        list.forEachIndexed{ i, item ->
            item.selected = i == pos
        }
    }
}