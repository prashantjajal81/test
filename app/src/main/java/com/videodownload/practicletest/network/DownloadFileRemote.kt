package com.videodownload.practicletest.network

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

object DownloadFileRemote {
    const val baseURL = "http://commondatastorage.googleapis.com/"
    fun download(url :String, apiCallback: ApiCallback){
        createService(RetrofitInterface::class.java,baseURL)
            .downloadFileWith(url)
            .enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    apiCallback.onFailure(t)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    Log.e("DownloadFileRemote", response.body().toString())
                    apiCallback.onSuccess(response.body())
                }

            })
    }
    private fun <T> createService(serviceClass: Class<T>, baseUrl: String): T {
        val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(OkHttpClient.Builder().build())
            .build()
        return retrofit.create(serviceClass)
    }
}