package com.videodownload.practicletest.network

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Streaming
import retrofit2.http.Url

interface ApiCallback{
    fun onSuccess(response:Any?)
    fun onProgressUpdate(response:Any?)
    fun onFailure(t: Throwable)
}
interface RetrofitInterface {
    @Streaming
    @GET
    fun downloadFileWith(@Url urlString: String): Call<ResponseBody>
}