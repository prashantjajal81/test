package com.videodownload.practicletest

import android.content.Context
import android.os.Environment
import android.util.Log
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.InputStreamReader

fun readFileFromAsset(context: Context): String {
    var reader: BufferedReader? = null
    var content = ""
    try {
        reader = BufferedReader(InputStreamReader(context.assets.open("VideoList.json"), "UTF-8"))
        var mLine: String? = ""
        mLine = reader.readLine()
        do {
            content += mLine
            mLine = reader.readLine()
        } while (mLine != null)

    } catch (e: IOException) {
        Log.e("VideoListActivity", e.message)
    } finally {
        if (reader != null) {
            try {
                reader.close()
            } catch (e: IOException) {
                Log.e("VideoListActivity", e.message)
            }
        }
    }
    Log.e("VideoListActivity", content)
    return content
}

private fun getStorageFileDirectory(): File? {
    return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)
}

private fun getFileNameFromUrl(url:String?):String?{
    val startIndex = url?.lastIndexOf("/") ?: 0
    val endIndex = url?.length ?: 0
    return url?.substring(startIndex, endIndex)
}

fun getFileObjectForVideoFileFromUrl(url: String?): File {
    val file = getStorageFileDirectory()
    if(file?.exists() == false){
        file.mkdir()
    }
    return File(file.toString(),
        getFileNameFromUrl(url))
}
//fun clearDocumentDir(){
//    getStorageFileDirectory()?.deleteRecursively()
//}