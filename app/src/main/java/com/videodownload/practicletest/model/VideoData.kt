package com.videodownload.practicletest.model

data class VideoData(
    val videos: List<VideoObj>
)