package com.videodownload.practicletest.model

data class VideoObj(
    val description: String,
    val sources: List<String>,
    val subtitle: String,
    val thumb: String,
    val title: String,
    var selected: Boolean = false
)