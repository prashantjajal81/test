package com.videodownload.practicletest.customview

//import com.videodownload.practicletest.model.Download
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.AsyncTask
import android.provider.Settings
import android.support.v4.content.FileProvider
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.videodownload.practicletest.GlideApp
import com.videodownload.practicletest.R
import com.videodownload.practicletest.getFileObjectForVideoFileFromUrl
import com.videodownload.practicletest.model.VideoObj
import com.videodownload.practicletest.network.ApiCallback
import com.videodownload.practicletest.network.DownloadFileRemote
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.viedeo_view.view.*
import okhttp3.ResponseBody
import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class MyVideoView : LinearLayout, LayoutContainer {
    private var totalFileSize: Int = 0
    override var containerView: View? = null
    var videoObj: VideoObj? = null
        set(value) {
            field = value
            tvVideoName.text = videoObj?.title
            if (isFileAvailable()) {
               showThumbOnly()
            } else {
                ivThumb.visibility = View.VISIBLE
                GlideApp.with(context)
                    .load("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/" + videoObj?.thumb)
                    .into(ivThumb)
                videoView.visibility = View.GONE
                progressVideo.visibility = View.GONE
                btnDownloadVideo.visibility = View.VISIBLE
                btnDownloadVideo.setOnClickListener {
                    checkForPermission()
                }
            }
        }

    private fun showThumbOnly() {
        btnDownloadVideo.visibility = View.GONE
        progressVideo.visibility = View.GONE
        videoView.visibility = View.GONE
        ivThumb.visibility = View.VISIBLE
        if (videoObj?.selected == true) {
            prepareVideoPlay()
        } else {
            GlideApp.with(context)
                .load("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/" + videoObj?.thumb)
                .into(ivThumb)
        }
    }

    private fun checkForPermission() {
        Dexter.withActivity(context as Activity?)
            .withPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    downloadVideo()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                    Log.e("MyVideoView", "onPermissionRationaleShouldBeShown")
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    if (response?.isPermanentlyDenied != false) {
                        showAlertForPermission()
                    }
                    Toast.makeText(
                        this@MyVideoView.context,
                        "Download video failed. Please allow permission for download",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }).check()
    }

    private fun downloadVideo() {
        progressVideo.visibility = View.VISIBLE
        btnDownloadVideo.visibility = View.GONE
//        val downloadFileName = getFileObjectForVideoFileFromUrl(videoObj?.sources?.get(0))
//        Log.e("sdcardPath", downloadFileName.toString())
        DownloadFileRemote.download(videoObj?.sources?.get(0) ?: "", object : ApiCallback {
            override fun onSuccess(response: Any?) {
                Log.e("downloadVideo", "onSuccess")
                downloadFile(response as ResponseBody)
            }

            override fun onProgressUpdate(response: Any?) {
                Log.e("downloadVideo", "onProgressUpdate")
            }

            override fun onFailure(t: Throwable) {
                Log.e("downloadVideo", "onFailure")
                progressVideo.visibility = View.GONE
                btnDownloadVideo.visibility = View.VISIBLE
            }
        })
    }

    @Throws(IOException::class)
    private fun downloadFile(body: ResponseBody) {
        val outputFile = getFileObjectForVideoFileFromUrl(videoObj?.sources?.get(0))

        object : AsyncTask<Void, Int, Void>() {
            override fun doInBackground(vararg params: Void?): Void? {
                var count: Int
                val data = ByteArray(1024 * 4)
                val fileSize = body.contentLength()
                val bis = BufferedInputStream(body.byteStream(), 1024 * 8)

                val output = FileOutputStream(outputFile)
                var total: Long = 0
                val startTime = System.currentTimeMillis()
                var timeCount = 1
                count = bis.read(data)
                if (count != -1) {
                    do {
                        total += count.toLong()
                        totalFileSize = (fileSize / Math.pow(1024.0, 2.0)).toInt()
                        val current = Math.round(total / Math.pow(1024.0, 2.0)).toDouble()

                        val progress = (total * 100 / fileSize).toInt()

                        val currentTime = System.currentTimeMillis() - startTime

//                        val download = Download()
//                        download.totalFileSize = totalFileSize

                        if (currentTime > 1000 * timeCount) {

//                            download.currentFileSize = current.toInt()
//                            download.progress = progress
                            Log.e("MyVideoView", "$progress")
                            publishProgress(progress)
//                    sendNotification(download)
                            timeCount++
                        }

                        output.write(data, 0, count)
                        count = bis.read(data)
                    } while (count != -1)
                }
                output.flush()
                output.close()
                bis.close()
                return null
            }

            override fun onProgressUpdate(vararg values: Int?) {
                super.onProgressUpdate(values.get(0))
                progressVideo.progress = values.get(0) ?: 0
            }

            override fun onPostExecute(result: Void?) {
                super.onPostExecute(result)
                showThumbOnly()
            }
        }.execute()

    }

    private fun onDownloadComplete(outputFile: File) {
        Log.e("MyVideoView", "onDownloadComplete")
        videoView.visibility = View.VISIBLE
        ivThumb.visibility = View.GONE
        btnDownloadVideo.visibility = View.GONE
        progressVideo.visibility = View.GONE
        val sharedFileUri = FileProvider.getUriForFile(this.context, context.packageName + ".fileprovider", outputFile)
        videoView.setVideoURI(sharedFileUri)
//        videoView.start()

        videoView.setOnPreparedListener(object : MediaPlayer.OnPreparedListener {
            override fun onPrepared(mp: MediaPlayer?) {
                mp?.isLooping = true
                videoView.start()
            }
        })
    }

    private fun showAlertForPermission() {
        fun goToAppSetting() {
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uri = Uri.fromParts("package", context.packageName, null)
            intent.data = uri
            context.startActivity(intent)
        }

        val alert = AlertDialog.Builder(context)
        alert.setTitle("Permission required")
        alert.setMessage("Please allow permission from setting for download video")
        alert.setPositiveButton("OK") { dialog, which ->
            goToAppSetting()
        }
        alert.setNegativeButton("Cancel", null)
        alert.create().show()
    }

    private fun prepareVideoPlay() {
        val outputFile = getFileObjectForVideoFileFromUrl(videoObj?.sources?.get(0))
        onDownloadComplete(outputFile)
    }

    private fun isFileAvailable(): Boolean {
        val file = getFileObjectForVideoFileFromUrl(videoObj?.sources?.get(0))
        return file.exists()
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init()
    }

    private fun init() {
        containerView = LayoutInflater.from(context).inflate(R.layout.viedeo_view, null)
        val param =
            LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        containerView?.layoutParams = param
        addView(containerView)
    }
}
